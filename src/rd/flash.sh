#!/sbin/sh

# Stock init.rc
INITRC=/tmp/rd/ramdisk/init.rc
# Vendor init..rc
VENDORINITRC=/vendor/etc/init/hw/init.kirin970.rc

# dump the existing ramdisk into an image we can work with
dd if=/dev/block/bootdevice/by-name/ramdisk of=/tmp/ramdisk.img

# Now back it up in case we need to uninstall it
cp /tmp/ramdisk.img /sdcard/

# make a temp work directory
mkdir /tmp/rd

# unpack our ramdisk
/tmp/unpackbootimg -i /tmp/ramdisk.img -o /tmp/rd/

# make another ramdisk work directory
mkdir /tmp/rd/ramdisk

# copy in our real ramdisk
cp /tmp/rd/ramdisk.img-ramdisk.gz /tmp/rd/ramdisk/
cd /tmp/rd/ramdisk/

# unpack it
gunzip -c /tmp/rd/ramdisk/ramdisk.img-ramdisk.gz | cpio -i

# remove our ramdisk images
rm -f /tmp/rd/ramdisk.img-ramdisk.gz
rm -f /tmp/rd/ramdisk/ramdisk.img-ramdisk.gz

# remove our old scripts, these will fail silently
rm -f /tmp/rd/ramdisk/sbin/kev_*
rm -f /tmp/rd/ramdisk/init.kevp75.rc

# copy in our init.rc file
cp /tmp/init.kevp75.rc /tmp/rd/ramdisk/

# copy in our scripts
cp -a /tmp/sbin/scripts/* /tmp/rd/ramdisk/sbin/
	
# copy in busybox
cp -a /tmp/sbin/others/busybox /tmp/rd/ramdisk/sbin/

# copy in bash
cp -a /tmp/sbin/others/bash* /tmp/rd/ramdisk/sbin/

# copy in our new fstabs
cp -a /tmp/sbin/others/fstab* /tmp/rd/ramdisk/

# Inject kevs tweaks
if ! grep -q 'kevp75' $INITRC; then
   sed -i '1i import /init.kevp75.rc' $INITRC
fi;

# remove the stock interactive tweaks from init.rc
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/timer_rate:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/timer_slack:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/min_sample_time:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/hispeed_freq:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/target_loads:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/go_hispeed_load:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/above_hispeed_delay:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/boost:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/input_boost:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/boostpulse_duration:d' $INITRC
sed -i '\:/sys/devices/system/cpu/cpufreq/interactive/io_is_busy:d' $INITRC

# remove the stock interactive tweaks from vendor init.kirin970.rc
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/go_hispeed_load:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/hispeed_freq:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/target_loads:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/above_hispeed_delay:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/min_sample_time:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/fast_ramp_down:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/boostpulse:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/target_loads:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu0/cpufreq/interactive/timer_slack:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu4/cpufreq/interactive/go_hispeed_load:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu4/cpufreq/interactive/hispeed_freq:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu4/cpufreq/interactive/target_loads:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu4/cpufreq/interactive/above_hispeed_delay:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu4/cpufreq/interactive/min_sample_time:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu4/cpufreq/interactive/fast_ramp_down:d' $VENDORINITRC
sed -i '\:/sys/devices/system/cpu/cpu4/cpufreq/interactive/timer_slack:d' $VENDORINITRC

chmod 750 /tmp/rd/ramdisk/init.kevp75.rc
chmod 755 /tmp/rd/ramdisk/sbin/busybox
chmod 755 /tmp/rd/ramdisk/sbin/bash
chmod 755 /tmp/rd/ramdisk/sbin/bashbug
chmod 755 /tmp/rd/ramdisk/sbin/kev_*

find . | cpio -o -H newc | gzip > /tmp/rd/ramdisk.img-ramdisk.gz

/tmp/mkbootimg --kernel /tmp/rd/ramdisk.img-zImage --ramdisk /tmp/rd/ramdisk.img-ramdisk.gz --cmdline "$(cat /tmp/rd/ramdisk.img-cmdline)" --base 0x10000000 --pagesize 2048 --ramdisk_offset 0x01000000 --tags_offset 0x00000100 --os_version "$(cat /tmp/rd/ramdisk.img-osversion)" --os_patch_level "$(cat /tmp/rd/ramdisk.img-oslevel)" -o /tmp/newramdisk.img
dd if=/tmp/newramdisk.img of=/dev/block/bootdevice/by-name/ramdisk
rm -r /tmp/rd/

