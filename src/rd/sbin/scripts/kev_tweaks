#!/sbin/bash

BB=/sbin/busybox;

LOG=/data/local/kev-tweaks.log

# check if we're already running.  if we are, exit
if [ -f /data/local/kev_tweaks.running ]; then
	exit 1;
fi;

# If we havent exitted, write a .running file out to make sure we aren't running multiple times
echo 1 > /data/local/kev_tweaks.running

echo "--------------------------------------------------------" >> $LOG
echo "--------------------------------------------------------" >> $LOG
echo "KEVS Tweaks Boot" >> $LOG
echo `date` >> $LOG
echo "--------------------------------------------------------" >> $LOG

# update local DNS servers to new cloudflare
$BB chmod 777 /system/etc/resolv.conf
echo "nameserver 1.1.1.1" > /system/etc/resolv.conf
echo "nameserver 1.0.0.1" >> /system/etc/resolv.conf
$BB chmod 644 /system/etc/resolv.conf
iptables -t nat -A OUTPUT -p tcp --dport 53 -j DNAT --to-destination 1.1.1.1:53
iptables -t nat -A OUTPUT -p udp --dport 53 -j DNAT --to-destination 1.0.0.1:53
iptables -t nat -I OUTPUT -p tcp --dport 53 -j DNAT --to-destination 1.1.1.1:53
iptables -t nat -I OUTPUT -p udp --dport 53 -j DNAT --to-destination 1.0.0.1:53
setprop net.eth0.dns1 1.1.1.1
setprop net.eth0.dns2 1.0.0.1
setprop net.dns1 1.1.1.1
setprop net.dns2 1.0.0.1
setprop net.ppp0.dns1 1.1.1.1
setprop net.ppp0.dns2 1.0.0.1
setprop net.rmnet0.dns1 1.1.1.1
setprop net.rmnet0.dns2 1.0.0.1
setprop net.rmnet1.dns1 1.1.1.1
setprop net.rmnet1.dns2 1.0.0.1
setprop net.pdpbr1.dns1 1.1.1.1
setprop net.pdpbr1.dns2 1.0.0.1

# Some cleaning
rm -f /cache/*.apk;
rm -f /cache/*.tmp;
rm -f /cache/recovery/*;
rm -f /data/*.log;
rm -f /data/*.txt;
rm -f /data/log/*.*;
rm -f /data/local/*.log;
rm -f /data/local/tmp/*.*;
rm -f /data/last_alog/*;
rm -f /data/last_kmsg/*;
rm -f /data/mlog/*;
rm -rf /data/tombstones/*;
rm -f /data/system/dropbox/*;
rm -f /data/system/usagestats/*.*;
chmod 700 /data/system/usagestats;
rm -rf /sdcard/LOST.DIR;

# Turn off logging
stop logd

# Disable Media Scanning
pm disable com.android.providers.media/com.android.providers.media.MediaScannerReceiver;

# Kill debugging
$BB sysctl -w vm.panic_on_oom=0
$BB sysctl -w kernel.panic_on_oops=0
$BB sysctl -w kernel.panic=0

# Perfect Mounts, this requires busybox.
$BB mount -o remount,noatime,noauto_da_alloc,nodiratime,barrier=0,nobh /system;
$BB mount -o remount,noatime,nodiratime /data;
$BB mount -o remount,noatime,noauto_da_alloc,nosuid,nodev,nodiratime,barrier=0,nobh /cache;
$BB mount -o remount,noatime,noauto_da_alloc,nosuid,nodev,nodiratime,barrier=0,nobh /vendor;
$BB mount -o remount,noatime,nodiratime,rw,errors=remount-ro /mnt/media_rw/*;

# Trim every week, and on boot
(
while true
do
	$BB mount -o remount,rw /system;
	$BB mount -o remount,rw /cache;
	$BB mount -o remount,rw /data;
	$BB mount -o remount,rw /vendor;
	$BB mount -o remount,rw /cust;
	$BB fstrim -v /system;
	$BB fstrim -v /data;
	$BB fstrim -v /cache;
	$BB fstrim -v /vendor;
	$BB fstrim -v /cust;
	$BB mount -o remount,ro /system;
	$BB mount -o remount,ro /vendor;
	$BB mount -o remount,ro /cust;
	sleep 10080 # a weeks worth of seconds :)
done
) &
